/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Communication over localhost UDP socket
 */

#include <err.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "lpus.h"

/** Service message, sent by clients */
#define READY   "client_ready"

/** UDP socket address struct */
static struct sockaddr_in addr = {
    .sin_family = AF_INET,
    .sin_addr.s_addr = INADDR_ANY,
    .sin_port = 0x8888, /**< port for this utility */
};

int comm_socket_open(void)
{
    int rc;
    int sock_fd;

    sock_fd =  socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd < 0)
        err(1, "socket open");

    /* Become a server, if there is no server yet o this port */
    rc = bind(sock_fd, (struct sockaddr*)&addr, sizeof(addr));
    if (rc < 0) {
        if (errno == EADDRINUSE) {
            /* We are client - send something, to establish UDP connection */
            rc = sendto(sock_fd, READY, strlen(READY), MSG_CONFIRM,
                        (struct sockaddr*)&addr, sizeof(addr));
            if (rc < 0)
                err(1, "socket sendto ready");
        } else {
            err(1, "socket bind");
        }
    }

    return sock_fd;
}

void comm_socket_send(int sock_fd, uint8_t* buf, size_t buf_size)
{
    /* send PUS payload (secondary header + user data) */
    ssize_t rc = sendto(sock_fd, buf, buf_size,
                        MSG_CONFIRM, (struct sockaddr*)&addr, sizeof(addr));
    if (rc < 0)
        err(1, "send PUS payload");
}

ssize_t comm_socket_receive(int sock_fd, uint8_t* buf, size_t buf_size)
{
    socklen_t addrlen = sizeof(addr);
    ssize_t size;

    /* receive secondary header */
    size = recvfrom(sock_fd, buf, buf_size,
                     MSG_WAITALL, (struct sockaddr*)&addr, &addrlen);
    if (size < 0)
        err(1, "receive PUS payload");

    /* if service message "ready" - ignore it and repeat recieve */
    if (strcmp((char*)buf, READY) == 0) {
        return comm_socket_receive(sock_fd, buf, buf_size);
    }

    return size;
}
