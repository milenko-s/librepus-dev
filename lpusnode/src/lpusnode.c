/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lpus_osdlp.h"

#define LPUSNODE_APID    0x2BC

extern int comm_socket_open(void);
extern void comm_socket_send(int sock_fd, uint8_t* buf, size_t buf_size);
extern ssize_t comm_socket_receive(int sock_fd, uint8_t* buf, size_t buf_size);
extern void struct_to_str(uint16_t sub, lpus_struct_t lpus_struct, char* str);
extern lpus_struct_t struct_gen(uint16_t sub);

static void* reader_thread_routine(void *param);

/**
 * Example program: transmit PUS messages between multiple instances of
 * this utility via localhost UDP socket.
 * Demonstrates encoding / decoding of all supported PUS services / subservices
 *
 * TODO: Add OSDLP library below PUS
 *
 * Usage:
 *
 * 1st instance (transmitter side):
 *
 * $ ./lpusinfo
 * > 0504
 * Sent: TM0504 44 high
 *
 * 2nd instance (receiver side):
 *
 * $ ./lpusnode
 * >
 * Received: TM0504 44 high
 */
int main(int argc, char* argv[])
{
    char cmd[256];
    uint8_t buf[256];
    static int sock_fd;
    int32_t rc;
    char str[256];
    pthread_t thread;

    (void)argc;
    (void)argv;

    /* Open communication over localhost UDP socket */
    sock_fd = comm_socket_open();

    /* Create receiver thread */
    rc = pthread_create(&thread, NULL, reader_thread_routine, &sock_fd);
    if (rc < 0) {
        errno = rc;
        err(1, "create thread");
    }

    /* Read user commands */
    for (;;) {
        unsigned long sub;
        lpus_struct_t lpus_struct= { };
        lpus_spacket_params_t spacket_p = {
            .apid = LPUSNODE_APID,
            .seq_flag = SPP_SEQ_FLAG_UNSEGMENTED,
            .seq_count = 0,
        };

        printf("> ");
        scanf("%s", cmd);
        /* quit command? */
        if (strcmp(cmd, "q") == 0)
            exit(0);
        /* hex integer command? - then it's a subservice number */
        sub = strtoul(cmd, NULL, 16);

        /* Encode PUS message */
        lpus_struct = struct_gen((uint16_t)sub);
        rc = lpus_spacket_encode(&spacket_p, sub, &lpus_struct,
                                 buf, sizeof(buf));
        if (rc < 0) {
            puts("unnown command");
            continue;
        }

        /* Send over socket */
        comm_socket_send(sock_fd, buf, rc);
        struct_to_str(sub, lpus_struct, str);
        printf("Sent: %s\n", str);
    }

    return 0;
}

static void* reader_thread_routine(void *param)
{
    int* sock_id_param = param;
    int sock_fd = (*sock_id_param);
    uint8_t buf[256];
    char str[256];
    int rc;

    for (;;) {
        uint16_t sub;
        ssize_t size;
        lpus_struct_t lpus_struct= { };
        lpus_spacket_params_t spacket_p = { };

        size = comm_socket_receive(sock_fd, buf, sizeof(buf));

        /* Decode PUS message */
        rc = lpus_spacket_decode(buf, size, &spacket_p, &sub, &lpus_struct);
        if (rc < 0) {
            puts("failed to decode");
            continue;
        }
        if (spacket_p.apid != LPUSNODE_APID) {
            printf("unknown APID 0x%x\n", spacket_p.apid);
            continue;
        }
        if (spacket_p.seq_flag != SPP_SEQ_FLAG_UNSEGMENTED) {
            printf("unknown sequence flag 0x%x\n", spacket_p.seq_flag);
            continue;
        }

        puts("");
        struct_to_str(sub, lpus_struct, str);
        printf("Received: %s\n", str);
        printf("> ");
    }

    return NULL;
}
