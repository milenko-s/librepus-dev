/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Generate pre-defined PUS structure for a given subservice
 */
#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "lpus.h"

/** Converted PUS structure to string */
void struct_to_str(uint16_t sub, lpus_struct_t lpus_struct, char* str)
{
    str[0] = '\0';

    switch (sub) {
    case TM0501_EV_INFO:
        sprintf(str, "TM0501 %x %.*s", lpus_struct.st05.tm0501.ev_id,
                                       (int)lpus_struct.st05.tm0501.data_size,
                                       (char*)lpus_struct.st05.tm0501.data);
        break;

    case TM0502_EV_LOW:
        sprintf(str, "TM0502 %x %.*s", lpus_struct.st05.tm0502.ev_id,
                                       (int)lpus_struct.st05.tm0502.data_size,
                                       (char*)lpus_struct.st05.tm0502.data);
        break;

    case TM0503_EV_MEDIUM:
        sprintf(str, "TM0503 %x %.*s", lpus_struct.st05.tm0503.ev_id,
                                       (int)lpus_struct.st05.tm0503.data_size,
                                       (char*)lpus_struct.st05.tm0503.data);
        break;

    case TM0504_EV_HIGH:
        sprintf(str, "TM0504 %x %.*s", lpus_struct.st05.tm0504.ev_id,
                                       (int)lpus_struct.st05.tm0504.data_size,
                                       (char*)lpus_struct.st05.tm0504.data);
        break;

    case TC0505_EN_REP:
        sprintf(str, "TC0505 ack: %x ev_list[%d]: { %x, %x, %x,... }",
                     lpus_struct.st05.tc0505.tc_ack.value,
                     lpus_struct.st05.tc0505.num,
                     lpus_struct.st05.tc0505.ev_list[0],
                     lpus_struct.st05.tc0505.ev_list[1],
                     lpus_struct.st05.tc0505.ev_list[2]);
        break;

    case TC0506_DIS_REP:
        sprintf(str, "TC0506 ack: %x ev_list[%d]: { %x, %x, %x,... }",
                     lpus_struct.st05.tc0506.tc_ack.value,
                     lpus_struct.st05.tc0506.num,
                     lpus_struct.st05.tc0506.ev_list[0],
                     lpus_struct.st05.tc0506.ev_list[1],
                     lpus_struct.st05.tc0506.ev_list[2]);
        break;

    case TC0507_REQ_DIS_LIST:
        sprintf(str, "TC0507 ack: %x", lpus_struct.st05.tc0507.tc_ack.value);
        break;

    case TM0508_RESP_DIS_LIST:
        sprintf(str, "TM0508 (n/a ack: %x) ev_list[%d]: { %x, %x, %x,... }",
                     lpus_struct.st05.tm0508.tc_ack.value,
                     lpus_struct.st05.tm0508.num,
                     lpus_struct.st05.tm0508.ev_list[0],
                     lpus_struct.st05.tm0508.ev_list[1],
                     lpus_struct.st05.tm0508.ev_list[2]);
        break;

    default:
        break;
    }
}

/** Generate pre-defined PUS structure for a given subservice */
lpus_struct_t struct_gen(uint16_t sub)
{
    lpus_struct_t lpus_struct= { };
    static event_id_t ev_list[16] = {
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10
    };

    switch (sub) {
    case TM0501_EV_INFO:
        lpus_struct.st05.tm0501.ev_id = 0x11;
        lpus_struct.st05.tm0501.data = "info";
        lpus_struct.st05.tm0501.data_size = 4;
        break;

    case TM0502_EV_LOW:
        lpus_struct.st05.tm0502.ev_id = 0x22;
        lpus_struct.st05.tm0502.data = "low";
        lpus_struct.st05.tm0502.data_size = 3;
        break;

    case TM0503_EV_MEDIUM:
        lpus_struct.st05.tm0503.ev_id = 0x33;
        lpus_struct.st05.tm0503.data = "medium";
        lpus_struct.st05.tm0503.data_size = 6;
        break;

    case TM0504_EV_HIGH:
        lpus_struct.st05.tm0504.ev_id = 0x44;
        lpus_struct.st05.tm0504.data = "high";
        lpus_struct.st05.tm0504.data_size = 4;
        break;

    case TC0505_EN_REP:
        lpus_struct.st05.tc0505.tc_ack.flags.accept_request = 1;
        lpus_struct.st05.tc0505.num = 8;
        lpus_struct.st05.tc0505.ev_list = ev_list;
        break;

    case TC0506_DIS_REP:
        lpus_struct.st05.tc0506.tc_ack.flags.accept_request = 0;
        lpus_struct.st05.tc0506.num = 11;
        lpus_struct.st05.tc0506.ev_list = ev_list;
        break;

    case TC0507_REQ_DIS_LIST:
        lpus_struct.st05.tc0507.tc_ack.flags.accept_request = 1;
        break;

    case TM0508_RESP_DIS_LIST:
        lpus_struct.st05.tm0508.num = 16;
        lpus_struct.st05.tm0508.ev_list = ev_list;
        break;

    default:
        break;
    }

    return lpus_struct;
}
